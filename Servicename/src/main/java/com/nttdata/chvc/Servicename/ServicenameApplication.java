package com.nttdata.chvc.Servicename;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicenameApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicenameApplication.class, args);
	}

}
