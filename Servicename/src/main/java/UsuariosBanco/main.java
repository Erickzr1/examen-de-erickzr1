package org.example;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Cliente clienteH = new Cliente("70840722","Carloss H.", "Vasquez Casas", Sexo.Masculino, (byte)28, Profesion.Ingeniero);
        Cliente clienteH2 = new Cliente("40840822","Julio Cesar", "Cisneros Palomino", Sexo.Masculino, (byte)29, Profesion.Politico);

        Cliente clienteM = new Cliente("70840722","Carloss H.", "Vasquez Casas", Sexo.Femenino, (byte)28, Profesion.Contador);

        CuentaCorriente cc1 = new CuentaCorriente("CC0001",clienteH,100.00,0.5);
        CuentaCorriente cc2 = new CuentaCorriente("CC0001",clienteM,100.00,0.5);
        CuentaCorriente cc3 = new CuentaCorriente("CC0001",clienteH,100.00,0.5);

        List<CuentaCorriente> listaCC = new ArrayList<>();
        listaCC.add(cc1);
        listaCC.add(cc2);
        listaCC.add(cc3);

        CuentaAhorros ca1 = new CuentaAhorros("CA0000",clienteH2,500.00,5.0,12);
        CuentaAhorros ca2 = new CuentaAhorros("CA0000",clienteH2,500.00,5.0,12);
        CuentaAhorros ca3 = new CuentaAhorros("CA0000",clienteH2,500.00,5.0,12);

        List<CuentaAhorros> listaCA = new ArrayList<>();
        listaCA.add(ca1);
        listaCA.add(ca2);
        listaCA.add(ca3);

    }

    public static String mostrarHombresMujeres(List<CuentaCorriente> listaCC,List<CuentaAhorros>listaCA){
        int contadorH = 0;
        int contadorM = 0;
        for (CuentaCorriente cc : listaCC) {
            if (cc.cliente.getSexo() == Sexo.Femenino){
                contadorM++;
            }
            if (cc.cliente.getSexo() == Sexo.Masculino){
                contadorH++;
            }
        }
        String cc = "Cuentas Corrientes:" + "Hombres:"+contadorH+"Mujeres"+contadorM;
        for (CuentaAhorros ca : listaCA) {
            if (ca.cliente.getSexo() == Sexo.Femenino){
                contadorM++;
            }
            if (ca.cliente.getSexo() == Sexo.Masculino){
                contadorH++;
            }
        }
        String ca = "Cuentas Corrientes:" + "Hombres:"+contadorH+"Mujeres"+contadorM;
        return cc+ca;
    }

    public static String mostrarPorProfesion(List<CuentaCorriente> listaCC,List<CuentaAhorros>listaCA){
        return "";
    }
}