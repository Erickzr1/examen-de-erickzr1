package UsuariosBanco;

public class cuentabancarias {
    private Double interesMensual;
    private Integer cantidadMeses;

    public CuentaAhorros(String nrocuenta, Cliente cliente, Double montoApertura, Double interesMensual, Integer cantidadMeses) {
        super(nrocuenta, cliente, montoApertura);
        this.interesMensual = interesMensual;
        this.cantidadMeses = cantidadMeses;
    }

    @Override
    public String toString() {
        return "CuentaAhorros{" +
                "interesMensual=" + interesMensual +
                ", cantidadMeses=" + cantidadMeses +
                ", nrocuenta='" + nrocuenta + '\'' +
                ", cliente=" + cliente +
                ", montoApertura=" + montoApertura +
                ", saldo=" + saldo +
                '}';
    }

    public Double calcularSaldo(){
        return montoApertura+(cantidadMeses*interesMensual);
    }
}






package org.example;

public abstract class CuentaBancaria {
    protected String nrocuenta;
    protected Cliente cliente;

    protected Double montoApertura;

    protected Double saldo;

    public CuentaBancaria() {
    }

    public CuentaBancaria(String nrocuenta, Cliente cliente, Double montoApertura) {
        this.nrocuenta = nrocuenta;
        this.cliente = cliente;
        this.montoApertura = montoApertura;
    }
}



package org.example;

public class CuentaCorriente extends CuentaBancaria{
    private Double interesProrrateado;

    public CuentaCorriente(String nrocuenta, Cliente cliente, Double montoApertura, Double interesProrrateado) {
        super(nrocuenta, cliente, montoApertura);
        this.interesProrrateado = interesProrrateado;
    }

    @Override
    public String toString() {
        return "CuentaAhorros{" +
                "interesProrrateado=" + interesProrrateado +
                ", nrocuenta='" + nrocuenta + '\'' +
                ", cliente=" + cliente +
                ", montoApertura=" + montoApertura +
                ", saldo=" + saldo +
                '}';
    }

    public Double calcularSaldo(){
        return montoApertura+(montoApertura*interesProrrateado);
    }
}





