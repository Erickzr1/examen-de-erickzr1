package org.example;

public abstract class CuentaBancaria {
    protected String nrocuenta;
    protected Cliente cliente;

    protected Double montoApertura;

    protected Double saldo;

    public CuentaBancaria() {
    }

    public CuentaBancaria(String nrocuenta, Cliente cliente, Double montoApertura) {
        this.nrocuenta = nrocuenta;
        this.cliente = cliente;
        this.montoApertura = montoApertura;
    }
}